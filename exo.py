import mysql.connector
import pandas as pd
import numpy as np
from datetime import datetime

def get_cursor(dictionary=False):
    connection = mysql.connector.connect(user="benjamin", password="coucou123", host="localhost", database="works")
    return connection, connection.cursor(dictionary=dictionary)

connection, cursor = get_cursor()

df = df = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="employees")
print(df)
print(df.columns)

df_1= pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="31-01-2024",header=1)
#print(df_1)

df_2 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="30-01-2024",header=1)
#print(df_2)

df_3 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="29-01-2024",header=1)
#print(df_3)

df_4 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="27-01-2024",header=1)
#print(df_4)

df_5 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="26-01-2024",header=1)
#print(df_5)

df_6 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="25-01-2024",header=1)
#print(df_6)

df_7 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="24-01-2024",header=1)
#print(df_7)

df_8 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="23-01-2024",header=1)
#print(df_8)

df_9 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="22-01-2024",header=1)
#print(df_9)

df_10 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="21-01-2024",header=1)
#print(df_10)

df_11 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="20-01-2024",header=1)
#print(df_11)

df_12 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="19-01-2024",header=1)
#print(df_12)

df_13 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="18-01-2024",header=1)
#print(df_13)

df_14 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="17-01-2024",header=1)
#print(df_14)

df_15 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="16-01-2024",header=1)
#print(df_15)

df_16 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="15-01-2024",header=1)
#print(df_16)

df_17 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="14-01-2024",header=1)
#print(df_17)

df_18 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="13-01-2024",header=1)
#print(df_18)

df_19 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="12-01-2024",header=1)
#print(df_19)

df_20 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="11-01-2024",header=1)
#print(df_20)

df_21 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="10-01-2024",header=1)
#print(df_21)

df_22 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="09-01-2024",header=1)
#print(df_22)

df_23 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="08-01-2024",header=1)
#print(df_23)

df_24 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="07-01-2024",header=1)
#print(df_24)

df_25 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="06-01-2024",header=1)
#print(df_25)

df_26 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="05-01-2024",header=1)
#print(df_26)

df_27 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="04-01-2024",header=1)
#print(df_27)

df_28 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="03-01-2024",header=1)
#print(df_28)

df_29 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="02-01-2024",header=1)
#print(df_29)

df_30 = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="01-01-2024",header=1)
print(df_30)

department={}

#listes_id = df['employee_id'].unique().astype(str)
#for salarie in listes_id:
   #cursor.execute("""insert into employee(id) values (%s);""", params=(salarie,))
#connection.commit()

#liste_id = df['id'].unique().astype(str)
#for id in liste_id:
    #cursor.execute("""insert into jour(id) values (%s);""", params=(id,))
#connection.commit()
listes_salarie = df[['employee_name', 'department']].values.tolist()
for salaries in listes_salarie:
    name, department = salaries
    cursor.execute("""INSERT INTO employee(name, departement) VALUES (%s, %s);""", (name, department))    
connection.commit()

#listes_department = df['department'].unique().astype(str)
#for department in listes_department:
   # cursor.execute("""insert into employee(departement) values (%s);""", params=(department,))
#connection.commit()

#df['shift_start_time'] = pd.to_datetime(df['shift_start_time'], format='%H:%M')
#liste_start = df['shift_start_time'].unique()
#print(liste_start)
#for start in liste_start:
    #print(start)
    #cursor.execute("""INSERT INTO jour(start) VALUES (%s);""", params=(start,))
#connection.commit()

#df['shift_end_time'] = pd.to_datetime(df['shift_end_time'], format='%H:%M')
#liste_end = df['shift_end_time'].unique()
#for end in liste_end:
    #cursor.execute("""INSERT INTO jour(end) VALUES (%s);""", params=(end,))
#connection.commit()

#listes_duration = df['break_duration'].astype(int)
#for duration in listes_duration:
    #cursor.execute("""insert into jour(duration) values (%s);""", params=(duration,))
#connection.commit()

#listes_heures = df['overtime_hours'].astype(int)
#for heures in listes_heures:
    #cursor.execute("""insert into jour(overtime) values (%s);""", params=(heures,))
#connection.commit()

#listes_location = df['location'].astype(str)
#for location in listes_location:
    #cursor.execute("""insert into jour(location) values (%s);""", params=(location,))
#connection.commit()
df_1['shift_start_time'] = pd.to_datetime(df_1['shift_start_time'], format='%H:%M')
df_1['shift_end_time'] = pd.to_datetime(df_1['shift_end_time'], format='%H:%M')

df_1['shift_start_time'] = df_1['shift_start_time'].dt.strftime('%H:%M')
df_1['shift_end_time'] = df_1['shift_end_time'].dt.strftime('%H:%M')

for index, row in df_1.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_2['shift_start_time'] = pd.to_datetime(df_2['shift_start_time'], format='%H:%M')
df_2['shift_end_time'] = pd.to_datetime(df_2['shift_end_time'], format='%H:%M')

for index, row in df_2.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_3['shift_start_time'] = pd.to_datetime(df_3['shift_start_time'], format='%H:%M')
df_3['shift_end_time'] = pd.to_datetime(df_3['shift_end_time'], format='%H:%M')

for index, row in df_3.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                          row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()
    
df_4['shift_start_time'] = pd.to_datetime(df_4['shift_start_time'], format='%H:%M')
df_4['shift_end_time'] = pd.to_datetime(df_4['shift_end_time'], format='%H:%M')

for index, row in df_4.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_5['shift_start_time'] = pd.to_datetime(df_5['shift_start_time'], format='%H:%M')
df_5['shift_end_time'] = pd.to_datetime(df_5['shift_end_time'], format='%H:%M')

for index, row in df_5.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_6['shift_start_time'] = pd.to_datetime(df_6['shift_start_time'], format='%H:%M')
df_6['shift_end_time'] = pd.to_datetime(df_6['shift_end_time'], format='%H:%M')

for index, row in df_6.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                         row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_7['shift_start_time'] = pd.to_datetime(df_7['shift_start_time'], format='%H:%M')
df_7['shift_end_time'] = pd.to_datetime(df_7['shift_end_time'], format='%H:%M')

for index, row in df_7.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_8['shift_start_time'] = pd.to_datetime(df_8['shift_start_time'], format='%H:%M')
df_8['shift_end_time'] = pd.to_datetime(df_8['shift_end_time'], format='%H:%M')

for index, row in df_8.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_9['shift_start_time'] = pd.to_datetime(df_9['shift_start_time'], format='%H:%M')
df_9['shift_end_time'] = pd.to_datetime(df_9['shift_end_time'], format='%H:%M')

for index, row in df_9.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_10['shift_start_time'] = pd.to_datetime(df_10['shift_start_time'], format='%H:%M')
df_10['shift_end_time'] = pd.to_datetime(df_10['shift_end_time'], format='%H:%M')

for index, row in df_10.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_11['shift_start_time'] = pd.to_datetime(df_11['shift_start_time'], format='%H:%M')
df_11['shift_end_time'] = pd.to_datetime(df_11['shift_end_time'], format='%H:%M')

for index, row in df_11.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_12['shift_start_time'] = pd.to_datetime(df_12['shift_start_time'], format='%H:%M')
df_12['shift_end_time'] = pd.to_datetime(df_12['shift_end_time'], format='%H:%M')

for index, row in df_12.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_13['shift_start_time'] = pd.to_datetime(df_13['shift_start_time'], format='%H:%M')
df_13['shift_end_time'] = pd.to_datetime(df_13['shift_end_time'], format='%H:%M')

for index, row in df_13.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_14['shift_start_time'] = pd.to_datetime(df_14['shift_start_time'], format='%H:%M')
df_14['shift_end_time'] = pd.to_datetime(df_14['shift_end_time'], format='%H:%M')

for index, row in df_14.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                  params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_15['shift_start_time'] = pd.to_datetime(df_15['shift_start_time'], format='%H:%M')
df_15['shift_end_time'] = pd.to_datetime(df_15['shift_end_time'], format='%H:%M')

for index, row in df_15.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_16['shift_start_time'] = pd.to_datetime(df_16['shift_start_time'], format='%H:%M')
df_16['shift_end_time'] = pd.to_datetime(df_16['shift_end_time'], format='%H:%M')

for index, row in df_16.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_17['shift_start_time'] = pd.to_datetime(df_17['shift_start_time'], format='%H:%M')
df_17['shift_end_time'] = pd.to_datetime(df_17['shift_end_time'], format='%H:%M')

for index, row in df_17.iterrows():
   cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                  params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_18['shift_start_time'] = pd.to_datetime(df_18['shift_start_time'], format='%H:%M')
df_18['shift_end_time'] = pd.to_datetime(df_18['shift_end_time'], format='%H:%M')

for index, row in df_18.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                     VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_19['shift_start_time'] = pd.to_datetime(df_19['shift_start_time'], format='%H:%M')
df_19['shift_end_time'] = pd.to_datetime(df_19['shift_end_time'], format='%H:%M')

for index, row in df_19.iterrows():
   cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_20['shift_start_time'] = pd.to_datetime(df_20['shift_start_time'], format='%H:%M')
df_20['shift_end_time'] = pd.to_datetime(df_20['shift_end_time'], format='%H:%M')

for index, row in df_20.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_21['shift_start_time'] = pd.to_datetime(df_21['shift_start_time'], format='%H:%M')
df_21['shift_end_time'] = pd.to_datetime(df_21['shift_end_time'], format='%H:%M')

for index, row in df_21.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                          row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_22['shift_start_time'] = pd.to_datetime(df_22['shift_start_time'], format='%H:%M')
df_22['shift_end_time'] = pd.to_datetime(df_22['shift_end_time'], format='%H:%M')

for index, row in df_22.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_23['shift_start_time'] = pd.to_datetime(df_23['shift_start_time'], format='%H:%M')
df_23['shift_end_time'] = pd.to_datetime(df_23['shift_end_time'], format='%H:%M')

for index, row in df_23.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                    VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_24['shift_start_time'] = pd.to_datetime(df_24['shift_start_time'], format='%H:%M')
df_24['shift_end_time'] = pd.to_datetime(df_24['shift_end_time'], format='%H:%M')

for index, row in df_24.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_25['shift_start_time'] = pd.to_datetime(df_25['shift_start_time'], format='%H:%M')
df_25['shift_end_time'] = pd.to_datetime(df_25['shift_end_time'], format='%H:%M')

for index, row in df_25.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                     VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_26['shift_start_time'] = pd.to_datetime(df_26['shift_start_time'], format='%H:%M')
df_26['shift_end_time'] = pd.to_datetime(df_26['shift_end_time'], format='%H:%M')

for index, row in df_26.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                     VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                          row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_27['shift_start_time'] = pd.to_datetime(df_27['shift_start_time'], format='%H:%M')
df_27['shift_end_time'] = pd.to_datetime(df_27['shift_end_time'], format='%H:%M')

for index, row in df_27.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                     VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_28['shift_start_time'] = pd.to_datetime(df_28['shift_start_time'], format='%H:%M')
df_28['shift_end_time'] = pd.to_datetime(df_28['shift_end_time'], format='%H:%M')

for index, row in df_28.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                     VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_29['shift_start_time'] = pd.to_datetime(df_29['shift_start_time'], format='%H:%M')
df_29['shift_end_time'] = pd.to_datetime(df_29['shift_end_time'], format='%H:%M')

for index, row in df_29.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                     VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()

df_30['shift_start_time'] = pd.to_datetime(df_30['shift_start_time'], format='%H:%M')
df_30['shift_end_time'] = pd.to_datetime(df_30['shift_end_time'], format='%H:%M')

for index, row in df_30.iterrows():
    cursor.execute("""INSERT INTO jour(start, end, duration, overtime, location) 
                      VALUES (%s, %s, %s, %s, %s);""",
                   params=(row['shift_start_time'], row['shift_end_time'], 
                           row['break_duration'], row['overtime_hours'], row['location']))
    
connection.commit()