drop database if exists works;
create database works;
use works;

CREATE TABLE employee(
    id int AUTO_INCREMENT PRIMARY KEY UNIQUE,
    name varchar(255) NOT NULL UNIQUE,
    departement varchar(50)
);

CREATE TABLE jour(
    id int AUTO_INCREMENT PRIMARY KEY UNIQUE,
    start time,
    end time,
    duration int,
    overtime int,
    location varchar(50)
);

