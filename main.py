import pandas as pd

# Tous le code

# je recup tous les employés dans un df
employes = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="employees")
# je garde que les colonnes qui m'intéresse
employes = employes[['employee_id', 'employee_name', 'department']]

# pour les données de travail
# j'ouvre le fichier
xls = pd.ExcelFile('employee-65f983e1d32eb892857506.xlsx')
# oh mon dieu je peux avoir la liste des page !!!!
print(xls.sheet_names) # Oh !
# une petite liste pour stocker mes 30 df (un par page)
days_list = []
# je parcours toutes les pages
for sheet in xls.sheet_names:
    # condition louche pour sauter la première page et le jour férié
    if "2024" in sheet and sheet != "28-01-2024":
        # je fait mon df du jour
        day_df = xls.parse(sheet, skiprows=1)
        # j'ajoute la date
        day_df["date"] = sheet
        # je le mets dans ma liste
        days_list.append(day_df)
# j'agrège tous les df en un seul
work = pd.concat(days_list)
# je renomme la colone id des employé pour pouvoir merge les df
employes.rename({'employee_id': 'id'}, axis=1, inplace=True)
# je merge ! (j'ajoute le nom de l'employé à chaque ligne)
total = pd.merge(work, employes, on="id")
# j'enregistre ca dans un csv pour pas avoir à tout refaire à chaque fois que je lance mon script
#total.to_csv('truc machin.csv')

# 13 lignes de code utile
# 1h (large) en faisant des copier coller de la doc de pandas et stackoverflow

xls = pd.ExcelFile('employee-65f983e1d32eb892857506.xlsx')
print(xls.sheet_names) # Oh !
days_list = []
for sheet in xls.sheet_names:
    if "2024" in sheet and sheet != "28-01-2024":
        day_df = xls.parse(sheet, skiprows=1)
        day_df["date"] = sheet
        days_list.append(day_df)

work = pd.concat(days_list)
print(work)

employes = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="employees")
employes = employes[['employee_id', 'employee_name', 'department']]

def get_absences(work_data_frames, employees_data_frame):
    result = {}
    for key, df in work_data_frames.items():
        merged_df = pd.merge(df, employees_data_frame, right_on="employee_id", left_on="id", how="right")
        result[key] = merged_df.loc[merged_df["id"].isnull(), ["employee_id", "employee_name", "department"]]
    return pd.concat(result)

# Charger les données des employés
employees_data = pd.read_excel("employee-65f983e1d32eb892857506.xlsx", sheet_name="employees")
employees_data = employees_data[['employee_id', 'employee_name', 'department']]

def get_absences(work_data_frames, employees_data_frame):
    result = {}
    for key, df in work_data_frames.items():
        merged_df = pd.merge(df, employees_data_frame, right_on="employee_id", left_on="id", how="right")
        result[key] = merged_df.loc[merged_df["id"].isnull(), ["employee_id", "employee_name", "department"]]
    return pd.concat(result)

work_data_frames = {}
xls = pd.ExcelFile("employee-65f983e1d32eb892857506.xlsx")
for sheet_name in xls.sheet_names:
    if "2024" in sheet_name and sheet_name != "28-01-2024":
        work_data = xls.parse(sheet_name, skiprows=1)
        work_data["date"] = sheet_name
        work_data_frames[sheet_name] = work_data

absent_employees = get_absences(work_data_frames, employees_data)
print(absent_employees)






