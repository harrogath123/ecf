# 1. Contexte de l'ecf.

Nous avons en tout trente tableaux représentant les trente jours du mois de janvier 2024 qui sont des jours travaillés. La journée du 28 janvier 2024 est un jour de repos pour tous les salariés.

# 1.1. Première question.

Question:

```
Liste des employées qui ont menti sur leurs heures supplémentaires et liste des employés ayant eu au moins une journée d’absence.
Il ne doivent pas être pris en compte pour les questions de la première partie.
```

Réponse:

```

```

Question 1:

SELECT e.id, e.name
FROM employee e
JOIN jour j ON e.id = j.id
WHERE j.overtime > j.duration;

SELECT e.id, e.name
FROM employee e
LEFT JOIN jour j ON e.id = j.id
WHERE j.id IS NULL;

Question 2:

SELECT e.name,
       SUM((j.duration * COALESCE(
           CASE e.departement
               WHEN 'Production' THEN 12
               WHEN 'Management' THEN 50
               WHEN 'Support' THEN 20
               WHEN 'RD' THEN 30
               ELSE 0
           END, 0))
           + (j.overtime * COALESCE(
           CASE e.departement
               WHEN 'Production' THEN 12 * 1.25
               WHEN 'Management' THEN 50 * 1.25
               WHEN 'Support' THEN 20 * 1.25
               WHEN 'RD' THEN 30 * 1.25
               ELSE 0
           END, 0))
           + (COALESCE(
           CASE j.location
               WHEN 'A' THEN 15
               WHEN 'B' THEN 10
               WHEN 'C' THEN 20
               ELSE 0
           END, 0))) AS total_cost
FROM employee e
JOIN jour j ON e.id = j.id
GROUP BY e.name
ORDER BY total_cost DESC
LIMIT 10;

SELECT e.name,
       SUM((j.duration * COALESCE(
           CASE e.departement
               WHEN 'Production' THEN 12
               WHEN 'Management' THEN 50
               WHEN 'Support' THEN 20
               WHEN 'RD' THEN 30
               ELSE 0
           END, 0))
           + (j.overtime * COALESCE(
           CASE e.departement
               WHEN 'Production' THEN 12 * 1.25
               WHEN 'Management' THEN 50 * 1.25
               WHEN 'Support' THEN 20 * 1.25
               WHEN 'RD' THEN 30 * 1.25
               ELSE 0
           END, 0))
           + (COALESCE(
           CASE j.location
               WHEN 'A' THEN 15
               WHEN 'B' THEN 10
               WHEN 'C' THEN 20
               ELSE 0
           END, 0))) AS total_cost
FROM employee e
JOIN jour j ON e.id = j.id
GROUP BY e.name
ORDER BY total_cost DESC
LIMIT 10;

SELECT e.name,
       SUM((j.duration * COALESCE(
           CASE e.departement
               WHEN 'Production' THEN 12
               WHEN 'Management' THEN 50
               WHEN 'Support' THEN 20
               WHEN 'RD' THEN 30
               ELSE 0
           END, 0))
           + (j.overtime * COALESCE(
           CASE e.departement
               WHEN 'Production' THEN 12 * 1.25
               WHEN 'Management' THEN 50 * 1.25
               WHEN 'Support' THEN 20 * 1.25
               WHEN 'RD' THEN 30 * 1.25
               ELSE 0
           END, 0))
           + (COALESCE(
           CASE j.location
               WHEN 'A' THEN 15
               WHEN 'B' THEN 10
               WHEN 'C' THEN 20
               ELSE 0
           END, 0))) AS total_cost,
       SUM(j.duration) AS total_hours_worked
FROM employee e
JOIN jour j ON e.id = j.id
GROUP BY e.name
ORDER BY total_cost DESC
LIMIT 10;

SELECT e.name,
       SUM((j.duration * COALESCE(
           CASE e.departement
               WHEN 'Production' THEN 12
               WHEN 'Management' THEN 50
               WHEN 'Support' THEN 20
               WHEN 'RD' THEN 30
               ELSE 0
           END, 0))
           + (j.overtime * COALESCE(
           CASE e.departement
               WHEN 'Production' THEN 12 * 1.25
               WHEN 'Management' THEN 50 * 1.25
               WHEN 'Support' THEN 20 * 1.25
               WHEN 'RD' THEN 30 * 1.25
               ELSE 0
           END, 0))
           + (COALESCE(
           CASE j.location
               WHEN 'A' THEN 15
               WHEN 'B' THEN 10
               WHEN 'C' THEN 20
               ELSE 0
           END, 0))) AS total_cost,
       SUM(j.duration) AS total_hours_worked
FROM employee e
JOIN jour j ON e.id = j.id
GROUP BY e.name
ORDER BY total_hours_worked ASC
LIMIT 10;

SELECT e.name,
       COUNT(*) AS total_pause_count
FROM employee e
JOIN jour j ON e.id = j.id
WHERE j.duration = 'pause'
GROUP BY e.name
ORDER BY total_pause_count DESC
LIMIT 10;

SELECT * FROM jour WHERE duration= 'pause';

SELECT e.name
FROM employee e
WHERE NOT EXISTS (
    SELECT 1
    FROM jour j
    WHERE j.id = e.id
    AND j.overtime <= j.duration);

SELECT e.name
FROM employee e
GROUP BY e.id
HAVING COUNT(*) = (
    SELECT COUNT(*)
    FROM jour j
    WHERE j.id = e.id
    AND j.overtime > j.duration
);
